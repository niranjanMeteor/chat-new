var http = require('http'),
  express = require('express'),
  channelServer = require('./lib/channel-server');


var app = express();
app.use(express.static(__dirname + '/public'));

var server = http.createServer(app).listen(8080); 
channelServer.listen(server);

app.get('/', function(req, res){
	console.log('new connection');
  res.sendFile(__dirname + '/index.html');
});

console.log('listening on port -> 8080');