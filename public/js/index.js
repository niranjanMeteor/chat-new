$(function(){

	var socket = io.connect();

	var globalUserData = undefined;

	var globalUserChannel = undefined;

	$('#choose-username').submit(function(e){
		e.preventDefault();
		var name = $('#username').val();
		socket.emit('prepare user', {name:name}, function(err){
			if (err) {
				$('#nick-error').text(err);
				$('#username').val('');
			} else {
				$('#username-container').hide();
				$('#channel-container').show();
				Materialize.toast("Welcome to the Eckovation Channel", 1500 );
			}
		});
	});

	$('#user-channel-new').submit(function(e){
		e.preventDefault();
		var name = $('#new_channel_name').val();
		console.log('in creating channel form');
		socket.emit('create new channel', {name : name, author : globalUserData}, function(err, msg){
			if (err) {
				$('#new-channel-error').text(err);
				$('#new_channel_name').val('');
			} else {
				Materialize.toast(msg, 1500 );
			}
		});
	});

	$('#user-channel-post').submit(function(e){
		e.preventDefault();
		var message = $('#channel_post').val();
		socket.emit('new message posted', {message : message, channel : globalUserChannel, user : globalUserData}, function(err){
			if (err) {
				console.log('error in posting on user channel');
				$('#user-channel-post-error').text(err);
			} else {
				Materialize.toast("your have succesfully posted your message", 1500 );
			}
			$('#channel_post').val('');
		});
	});

	$('#channel-table').on('click', '.channel-status',function(e) {
		e.preventDefault();
		var channel_id = event.target.id;
		socket.emit('channel subscription',{channel_id : channel_id, user : globalUserData}, function(err) {
			if (err) {
				console.log('error in subscribing channel');
				throw err;
			} else {
				Materialize.toast("Subscription Updated Successfull", 1500 );
			}
		});
	});

	socket.on('user prepared', function(data) {
		globalUserData = data.user; 
		globalUserChannel = data.user_channel;
		displayWelcomeText(data.user);
		handleUserChannelBox(data.user_channel);
		displayAllChannels(data.channels);
	});

	socket.on('user channel created' , function(data) {
		globalUserChannel = data.channel;
		handleUserChannelBox(data.channel);
	});

	socket.on('new channel subscribed', function(data) {
		displayAllChannels(data.channels);
	});

	socket.on('update channels list', function(data) {
		socket.emit('fetch channels', {user : globalUserData}, function(err) {
			if (err) {
				throw err;
			} else {
				console.log('channels updated succesfully');
			}
		})
	});

	socket.on('new message', function(data) {
		handleNewMessage(data.message);
		Materialize.toast("New Message From -->> "+data.message.user.name+"", 700 );
	});

	function handleNewMessage(message) {
		console.log('in handle message - >',message.channel.name);
		var html = '';
		html += '<tr>';
		html += '<td>'+ message.channel.name +'</td>';
		html += '<td>'+ message.user.name +'</td>';
		html += '<td>'+ message.message +'</td>';
		html += '</tr>';
		$('#message-table-body').append(html);
	}

	function displayWelcomeText(user) {
		console.log(user);
		var html = '';
		html += '<span class="welcome-text">Welcome,  </span>' + user.name;
		$('#welcome-text').append(html);
	}

	function handleUserChannelBox(user_channel) {

		if (user_channel.id === null || user_channel.id === 'undefined') {
      $('#new-channel-body').show();
      $('#new-post-body').hide();
		} else {
   		$('#new-channel-body').hide();
      $('#new-post-body').show();
    	$('#new-post-username').html('Your Channel -- <span style="color : #8e24aa">'+user_channel.name+'</span>');
		}
	}

	function displayAllChannels(channels) {
		var html = '';

		if ( ! channels.length) {
			html = '<h4 class="center" style="margin-top : 60px;">No Channel Available To Join</h4>';
			$('#channel-table').html(html);
			return;
		}
		html += '<thead><tr><th>Channel</th><th>Author</th><th>Title</th><th>Add / Rem</th></tr></thead>';
    html += '<tbody id="user_channels">'

		for (var i = 0 ; i < channels.length ; i++) {
			html += '<tr>';
			html += '<td>' + (i+1) +'</td>';
			html += '<td>' + channels[i].author+ '</td>';
			html += '<td>' + channels[i].title + '</td>';
			html += '<td><button class="channel-status btn-floating btn-large waves-effect waves-light purple darken-1">';
			if (channels[i].subscribed) 
				html += '<i class="mdi-content-remove"  id="'+channels[i].id+'"></i>';
			else html += '<i class="mdi-content-add"  id="'+channels[i].id+'"></i>';
			html += '</button></td>'
			html += '</tr>';
		}

		html += '</tbody>';
		$('#channel-table').html(html);
	}

	$(document).ready(function(){
    $('ul.tabs').tabs();
    $('#username-container').show();
		$('#channel-container').hide();

  });
});
