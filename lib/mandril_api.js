var mandril_client = require('node-mandrill')('p6UzMV3YxMXLgeb-1jipFw');

exports.send = function (data, cb) {
	var message_text = data.user.name+'<'+data.channel.name+'> posted : '+data.message;
	mandril_client('/messages/send', {
	    message: {
	        to: [{email: "vimal.kumar@juspay.in", name: "Niranjan"}],
	        from_email: "niranjan.cs08@gmail.com",
	        subject: "New Message On Channel: "+data.channel.name,
	        text: message_text
	    }
	}, function(error, response)
	{
	    //uh oh, there was an error
	    if (error) cb( JSON.stringify(error) , null);

	    //everything's good, lets see what mandrill said
	    else cb(null, response);
	});
}