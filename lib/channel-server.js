var socketio = require('socket.io');
var db = require('./channel-db');
var mandrill_client = require('./mandril_api');
var io;
// maps socket.id to user's nickname
var user_sockets = [];

exports.listen = function(server){
  io = socketio.listen(server);
  io.sockets.on('connection', function(socket){
    prepareUserHome(socket);
    createNewChannel(socket);
    handleChannelSubscription(socket);
    fetchUserChannels(socket);
    handleNewMessage(socket);
    handleClientDisconnections(socket)
  });
};

function prepareUserHome(socket){
  socket.on('prepare user', function(data, cb) {
  	console.log('prepareUserHome : preparing User');
  	var allChannels = [];
  	db.ifUserExists(data, function(err, doc){
  		if (err){
  			throw err;
  		} else if (doc) {
  			console.log('prepareUserHome : user already existing is -->',doc._id);
  			db.getAllChannels(doc._id, function(err, all_channels) {
  				if (err) {
  					throw err;
  				} else {
  					db.getUserOwnChannel(doc._id, function(err, user_own_channel) {
  						if (err) {
  							throw err;
  						} else {
		  					user_sockets[doc._id] = socket;
    						cb(null);
    						io.to(socket.id).emit('user prepared', {user : {id : doc._id, name : doc.name, email:doc.email}, channels: all_channels, user_channel : user_own_channel});
  						}
  					});
  				}
  			});
  		} else {
  			db.saveUser(data, function(err, doc) {
  				if (err) {
  					cb(err);
  					return;
  				} else {
  					console.log('prepareUserHome : user id created is -->',doc._id);
  					db.getAllChannelsForNewuser(function(err, all_channels) {
		  				if (err) {
		  					throw err;
		  				} else {
		  					db.getUserOwnChannel(doc._id, function(err, user_own_channel) {
		  						if (err) {
		  							throw err;
		  						} else {
		  							user_sockets[doc._id] = socket;
		    						cb(null);
		    						io.to(socket.id).emit('user prepared', {user : {id : doc._id, name : doc.name, email:doc.email}, channels: all_channels, user_channel : user_own_channel});
		  						}
		  					});
		  				}
		  			});
  				}
  			});
  		}

  	});
  });
}

function createNewChannel(socket) {
	socket.on('create new channel', function(data, cb) {
		db.saveChannel({name : data.name, author : data.author}, function(err, channel) {
			if (err) {
				throw err;
			} else {
				if (channel.exists) {
					cb(null,'Channel Already Exists');
				}
				else {
					cb(null,'New Channel Successfully Created');
					io.to(socket.id).emit('user channel created', {channel : channel});
					io.sockets.emit('update channels list',{});
				}
			}
		})
	});
}

function handleChannelSubscription(socket) {
	socket.on('channel subscription' , function(data, cb) {
		db.manageSubscription(data, function(err) {
			if (err) {
				throw err;
			} else {
				db.getAllChannels(data.user.id, function(err, all_channels) {
					if (err) {
						throw err;
					} else {
						cb(null);
		    		io.to(socket.id).emit('new channel subscribed',{channels: all_channels});
					}
				});
			}
		});
	});
}

function fetchUserChannels(socket) {
	socket.on('fetch channels', function(data, cb) {
		db.getAllChannels(data.user.id, function(err, all_channels) {
			if (err) {
				throw err;
			} else {
				cb(null);
    		io.to(socket.id).emit('new channel subscribed',{channels: all_channels});
			}
		});
	});
}

function handleNewMessage(socket) {
	socket.on('new message posted', function(data, cb) {
		db.saveMessage(data, function(err, doc) {
			if (err) {
				throw err;
			} else {
				cb(null);
				db.getChannelSubscribers(data.channel.id, function(err, subscribers) {
					console.log('subsribers are ->',subscribers);
					for (var i=0 ; i < subscribers.length ; i++) {
						if ( user_sockets[subscribers[i].id] ) {
							console.log('message on socket');
							io.to(user_sockets[subscribers[i].id].id).emit('new message', {message : data});
						} else {
							console.log('message on email');
							mandrill_client.send(data, function(err, response) {
								if(err) {
									console.log('email not sent');
								} else {
									console.log('email succesfully');
								}
							});
						}
					}
				});
			}
		});
	});
}

function handleClientDisconnections(socket){
  socket.on('disconnect', function(){
    var ind = user_sockets.indexOf(socket.id);
    delete user_sockets[ind];
    console.log('a user disconnected');
  });
}
