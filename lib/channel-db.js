var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/eckovation', function(err){
	if(err) {
		console.log(err);
	} else {
		console.log('Connected to mongodb!');
	}
});



/************************ database collections Schemas ***************************/
var messageSchema = mongoose.Schema({
	channel: {id :String, name : String},
	author : {id: String, name : String},
	msg: String,
	created: {type: Date, default: Date.now}
});

var Message = mongoose.model('Message', messageSchema);

var userSchema = mongoose.Schema({
	name : String,
	email : String,
	created: {type: Date, default: Date.now},
	//channels_subsribed : {type: Array, "default": []}
});

var User = mongoose.model('User', userSchema);

var channelSchema = mongoose.Schema({
	name : String,
	author : {id: String, name : String},
	subsribers : {type : Array, "default" : []}
});

var Channel = mongoose.model('Channel', channelSchema);



/********************* methods exported for handling databse changes ***************************/
exports.saveUser = function(data, cb) {
	var newUser = new User({name: data.name, email: data.email});
	newUser.save(function(err, doc){
		cb(err,doc);
	});
};

exports.saveChannel = function(data, cb) {
	Channel.find({name : data.name}, function(err, channel) {
		if (err) {
			throw err;
		} else if (channel.length > 0) {
			cb(err, {id : channel._id, name : channel.name, exists : true});
		} else {
			var newChannel = new Channel({name : data.name, author : data.author, subsribers : data.author});
			newChannel.save(function(err, doc){
				cb(err, {id : doc._id, name : doc.name, exists : false});
			});
		}
	});
};

exports.saveMessage = function(data, cb){
	var newMsg = new Message({msg: data.msg, author: data.author, channel : data.channel});
	newMsg.save(function(err){
		cb(err);
	});
};

exports.ifUserExists = function(data, cb) {
	User.findOne({name : data.name}, function(err, docs){
		cb(err, docs);
	});
};

exports.getAllChannels = function(user_id, cb) {
	var allChannels = [];

	Channel.find({  }, function(err, channels) {
		if (err) {
			throw err;
		} else {
			for (var i = 0; i < channels.length; i++) {
				allChannels.push({
					id : channels[i]._id,
					author : channels[i].author.name,
					title : channels[i].name,
					subscribed : checkIfSubscribed(user_id, channels[i].subsribers)
				});
			}
		}
		cb(err,allChannels);
	});
};

function checkIfSubscribed(user_id, subsribers) {
	for ( var j = 0 ; j < subsribers.length ; j++) {
		if (subsribers[j].id == user_id) {
			return true
		}
	}
	return false;
}

exports.getAllChannelsForNewuser = function(cb) {
	var allChannels = [];

	Channel.find({}, function(err, channels) {
		if (err) {
			throw err;
		} else {
			for (var i = 0; i < channels.length; i++) {
				allChannels.push({
					id : channels[i]._id,
					author : channels[i].author.name,
					title : channels[i].name,
					subscribed : false
				});
			}
		}
		cb(err,allChannels);
	});
};

exports.getUserOwnChannel = function(user_id, cb) {
	Channel.findOne({"author.id" : user_id}, function(err, channel) {
		if (err) {
			throw err;
		} else if ( channel ) {
			 cb(err, {id : channel._id, name : channel.name});
		} else   cb(err, {id : null, name : null});
	})
};

exports.manageSubscription = function(data, cb) {
	Channel.findOne({_id : data.channel_id}, function(err, doc) {
		if (err) {
			throw err;
		} else if ( checkIfSubscribed(data.user.id, doc.subsribers) ) {
			Channel.update({_id : data.channel_id}, {$pull : {"subsribers" : {"id" : data.user.id}}},function(err){
				cb(err);
			});
		} else {
			Channel.update({_id : data.channel_id}, {$push : {"subsribers" : data.user}},function(err){
				cb(err);
			});
		}
	});
}

exports.getChannelSubscribers = function(data, cb) {
	Channel.findOne({_id : data}, function(err, doc) {
		if (err) {
			throw err;
		} else {
			cb(err, doc.subsribers);
		}
	})
}
